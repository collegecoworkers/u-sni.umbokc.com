<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Item,
	Favorit,
	User
};

class SiteController extends Controller
{

	public function __construct(){
	}

	public function Index() {
		return view('index')->with([
			'items' => Item::orderBy('id', 'desc')->limit(8)->get(),
		]);
	}
	public function Account() {
		$fvs = Favorit::getsBy(['user_id' => User::curr()->id]);

		$ids = [];
		foreach ($fvs as $item ) $ids[] = $item->item_id;

		$items = Item::whereIn('id', $ids)->get();

		return view('account')->with([
			'items' => $items,
		]);
	}
	public function All() {
		return view('item.all')->with([
			'items' => Item::orderBy('id', 'desc')->get(),
		]);
	}
	public function Item($id) {
		$item = Item::getBy('id', $id);
		return view('item.view')->with([
			'items' => Item::all(),
			'model' => $item,
		]);
	}
}
