<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Item,
	Favorit,
	User
};

class ItemController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}


	public function Add() {
		$item = new Item();
		return view('item.add')->with([
			'model' => $item,
		]);
	}
	public function Edit($id) {
		$item = Item::getBy('id', $id);
		return view('item.edit')->with([
			'model' => $item,
		]);
	}
	public function EditImg($id) {
		$item = Item::getById($id);
		return view('item.editimg')->with([
			'model' => $item,
		]);
	}
	public function ToFavorite($id) {
		$item = Item::getBy('id', $id);
		$fw = new Favorit;
		$fw->item_id = $item->id;
		$fw->user_id = User::curr()->id;
		$fw->save();
		return redirect()->back();
	}
	public function Create(Request $request) {

		$model = new Item();

		$model->title = request()->title;
		$model->code = request()->code;

		$model->save();

		return redirect('/admin');
	}
	public function Update($id, Request $request) {
		$model = Item::where('id', $id)->first();

		$model->title = request()->title;
		$model->code = request()->code;

		$model->save();
		return redirect('/admin');
	}

	public function UpdateImg($id, Request $request) {
		$model = Item::where('id', $id)->first();

		request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);
		$imageName = time().'.'.request()->image->getClientOriginalExtension();
		request()->image->move(public_path('images'), $imageName);
		$model->img = $imageName;
		$model->save();
		return redirect()->to('/');
	}
	public function Delete($id) {
		Item::where('id', $id)->delete();
		return redirect('/admin');
	}
}
