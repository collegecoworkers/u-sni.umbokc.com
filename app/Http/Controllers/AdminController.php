<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Item,
	User
};

class AdminController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Admin() {
		$users = User::all();
		$items = Item::all();
		return view('admin.admin')->with([
			'users' => $users,
			'items' => $items,
		]);
	}

	public function EditUser($id) {
		$user = User::where('id', $id)->first();
		return view('admin/edit-user')->with([
			'model' => $user,
		]);
	}
	public function UpdateUser($id, Request $request) {
		$model = User::where('id', $id)->first();

		$model->full_name = request()->full_name;
		$model->name = request()->name;
		$model->email = request()->email;
		$model->is_admin = request()->is_admin;

		$model->save();
		return redirect()->to('/admin');
	}
	public function DeleteUser($id) {
		User::where('id', $id)->delete();
		return redirect()->to('/admin');
	}

}
