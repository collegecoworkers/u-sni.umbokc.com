<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class MyModel extends Model{
  protected static $allArr_field = 'title';
  public static function allArr(){
    return F::toArr(self::all(), static::$allArr_field, 'id');
  }
  public static function getById($val){
    return self::getBy('id', $val);
  }
  public static function getBy($col, $val = null){
    if(is_array($col)) return self::queryBy($col)->first();
    else return self::queryBy([$col => $val])->first();
  }
  public static function getsBy($col, $val = null){
    if(is_array($col)) return self::queryBy($col)->get();
    else return self::queryBy([$col => $val])->get();
  }
  public static function queryBy($arr){
    return self::where($arr);
  }
}
