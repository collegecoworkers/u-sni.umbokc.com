<?php
namespace App;

class Item extends MyModel{
  public $timestamps = false;

  public function getImage() {
    return ($this->img != '') ?
      '/images/' . $this->img :
      'http://placehold.it/153x102';
  }

}
