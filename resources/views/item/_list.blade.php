@php
use App\{
	User,
	Favorit
};
@endphp
<ul>
	@php
		$i = 0;
	@endphp
	@foreach ($items as $item)
		@continue(isset($model) and $model->id == $item->id)
		<li>
			<div class="vediodiv">
				<a href="/item/{{ $item->id }}"><img class="the-img" src="{{ $item->getImage() }}" alt="" /></a>
				@auth
					@if (Favorit::getBy(['item_id' => $item->id, 'user_id' => User::curr()->id ]) == null)
					<a title="Добавить в избранное" href="/to-favorite/{{ $item->id }}" class="addtoplaylist"><img src="/assets/images/plusicon.png" alt="" /></a>
					@endif
				@endauth
			</div>
			<div class="clear"></div>
			<p class="ttle"><a class="colr2" href="/item/{{ $item->id }}">{{ $item->title }}</a></p>
		</li>
	@php
		$i++;
	@endphp
	@if ($i % 4 == 0)
		<div class="clear"></div>
	@endif
	@endforeach
</ul>
<style ea-s='d:n'>
	.the-img{
		width: 151px;
		height: 100px;
		position: relative;
		top: -3px;
		left: 0px;
	}
</style>
