@extends('layout.app')
@section('content')


<div class="">
	<div class="mostcontainer">
		<h3>Добавить трансляцию</h3>
		<br>
		<form method="post" action="/create">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="id__title">Название</label>
				<input type="text" class="form-control" id="id__title" name="title" placeholder="Название" required="">
			</div>
			<div class="form-group">
				<label for="id__code">Код</label>
				<textarea class="form-control" id="id__code" name="code" placeholder="Код" required=""></textarea>
			</div>
			<button type="submit" class="btn btn-success">Отправить</button>
		</form>
	</div>
</div>
@endsection
