@extends('layout.app')
@section('content')

<div class="">
	<div class="mostcontainer">
		<h2>Изменить данные</h2>
		<br>
		<form method="post" action="/update/{{ $model->id }}" >
			{{ csrf_field() }}
			<div class="form-group">
				<label for="id__title">Название</label>
				<input type="text" class="form-control" id="id__title" name="title" placeholder="Название" required="" value="{{ $model->title }}">
			</div>
			<div class="form-group">
				<label for="id__code">Код</label>
				<textarea class="form-control" id="id__code" name="code" placeholder="Код" required="" >{{ $model->code }}</textarea>
			</div>
			<button type="submit" class="btn btn-success">Сохранить</button>
		</form>
	</div>
</div>


@endsection
