@extends('layout.app')
@section('content')
<style>
.the-contentdiv > *, .the-contentdiv *{
	width: 617px !important;
	height: 324px !important;
}
.vedio_banner {
	width: 646px;
	height: 355px;
}
</style>
<div class="">
	<div class="mostcontainer">
		<h2>{{ $model->title }}</h2>
		<div class="vedio_banner">
			<div id="slider2" style="height:324px; overflow:hidden ">
				<div class="contentdiv the-contentdiv">
					{!! $model->code !!}
				</div>
			</div>
			<script type="text/javascript" src="js/slider.js"></script>
		</div>
		<div class="clear"></div>  
	</div>
	<div class="mostcontainer">
		<h2>Еще трансляции</h2>
		@include('item._list')
	</div>
	<div class="clear"></div>
</div>

@endsection
