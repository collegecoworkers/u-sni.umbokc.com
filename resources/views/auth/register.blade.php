@extends('layout.app')
@section('content')

<h2 class="pd11 pd2">Регистрация</h2>
<div class="continnesec" >
	<form class="form" method="post" action="{{ route('register') }}">
		{{ csrf_field() }}
		<div class="frmlft_sec">
			<ul>

				<li><input name="full_name" placeholder="Ваше полное имя" type="text" /></li>
				@if ($errors->has('full_name')) <li><span class="help-block"><strong>{{ $errors->first('full_name') }}</strong></span> </li>@endif

				<li><input name="name" placeholder="Логин" type="text" /></li>
				@if ($errors->has('name')) <li><span class="help-block"><strong>{{ $errors->first('name') }}</strong></span> </li>@endif

				<li><input name="email" placeholder="Email" type="text" /></li>
				@if ($errors->has('email')) <li><span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> </li>@endif

				<li><input name="password" placeholder="Пароль" type="password" /></li>
				@if ($errors->has('password')) <li><span class="help-block"><strong>{{ $errors->first('password') }}</strong></span></li> @endif

				<li><input name="password_confirmation" placeholder="Повторите пароль" type="password" /></li>
				@if ($errors->has('password_confirmation')) <li><span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span></li> @endif

			</ul>
			<input class="btn" value="Зарегистрироваться" type="submit" />
			<a href="{{ route('login') }}">Вход</a>
		</form>
	</div>
</div>

@endsection
