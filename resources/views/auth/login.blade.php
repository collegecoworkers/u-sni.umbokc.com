@extends('layout.app')
@section('content')
<h2 class="pd11 pd2">Вход</h2>
<div class="continnesec" >
	<form class="form" method="post" action="{{ route('login') }}">
		{{ csrf_field() }}
		<div class="frmlft_sec">
			<ul>
				<li><input name="email" placeholder="Email" type="text" /></li>
				@if ($errors->has('email')) <li><span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> </li>@endif
				<li><input name="password" placeholder="Пароль" type="password" /></li>
				@if ($errors->has('password')) <li><span class="help-block"><strong>{{ $errors->first('password') }}</strong></span></li> @endif
			</ul>
			<input class="btn" value="Войти" type="submit" />
			<a href="{{ route('register') }}">Регистрация</a>
		</form>
	</div>
</div>
@endsection
