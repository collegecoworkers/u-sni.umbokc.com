@php
	use App\{User};
@endphp
<!DOCTYPE html>
<html lang="en" ea>
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=2">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('assets/css/ddsmoothmenu.css') }}" />

	<script src="http://cdn.umbokc.com/ea/src/ea.js?v=2"></script>

	<script src="{{ asset('assets/js/jquery.min.js')}}"></script>
	<script src="{{ asset('assets/js/ddsmoothmenu.js')}}"></script>
	<script src="{{ asset('assets/js/menu.js')}}"></script>
	<script src="{{ asset('assets/js/contentslider.js')}}"></script>
	<script src="{{ asset('assets/js/Tahoma_400-Tahoma_700.font.js')}}"></script>

</head>
<body>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
	<div id="wrapper_sec">
		<div id="masthead">
			<div class="logo"><a href="/" style="color: #fff; font-size: 20px; line-height: 50px">Трансляция мира</a></div>
			<div class="header_rightsec" style="width: auto;">
				<div class="rightsec_1">
					<div class="navigation">
						<div id="smoothmenu1" class="ddsmoothmenu">
							<ul>
								<li><a class="home" href="/">Главная</a></li>
								<li><a href="/all">Все трансляции</a></li>
								@auth
									<li><a href="/account" >Личный кабинет</a></li>
									@if (User::isAdmin())
										<li><a href="/admin" >Админ панель</a></li>
									@endif
									<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a></li>
								@endauth
								@guest
									<li><a href="{{ route('login') }}" >Вход</a></li>
								@endguest
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="clear" ></div>
		</div>
		<div id="content_sec">

			@yield('content')

			<div class="clear"></div>
		</div>
	</div>

	<div id="bottom_sec">
		<div id="botbarbg">
			<div class="innercontainer">
				<div class="copy">&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</div>
			</div>
		</div>
	</div>

</body>
</html>
