@php
	use App\{
		User,
		Favorit
	};
@endphp
@extends('layout.app')
@section('content')

<div class="">
	<div class="mostcontainer">
		<h2>Избранное</h2>
		<ul>
			@foreach ($items as $item)
			<li>
				<div class="vediodiv">
					<a href="/item/{{ $item->id }}"><img class="the-img" src="{{ $item->getImage() }}" alt="" /></a>
					@auth
						@if (Favorit::getBy(['item_id' => $item->id, 'user_id' => User::curr()->id ]) == null)
							<a href="/to-favorite/{{ $item->id }}" class="addtoplaylist"><img src="/assets/images/plusicon.png" alt="" /></a>
						@endif
					@endauth
				</div>
				<div class="clear"></div>
				<p class="ttle"><a class="colr2" href="/item/{{ $item->id }}">{{ $item->title }}</a></p>
			</li>
			@endforeach
		</ul>
	</div>
	<div class="clear"></div>
</div>
<style ea-s='d:n'>
.the-img{
	width: 151px;
	height: 100px;
	position: relative;
	top: -3px;
	left: 0px;
}
</style>
@endsection
