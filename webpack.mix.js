let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
// mix.disableSuccessNotifications();
// mix.stylus('resources/assets/styl/app.styl', 'public/css');
mix.js('resources/assets/js/app.js', 'public/js')
// .stylus('resources/assets/styl/app.styl', 'public/css', {
// 		use: [
// 				require('rupture')(),
// 				require('nib')(),
// 				require('jeet')()
// 		],
// 		import: [
// 				'~nib/index.styl',
// 				'~jeet/jeet.styl'
// 		]
// })
.sourceMaps().then(function (stats) {
	// if (!stats.hasErrors() || !stats.hasWarnings()) {
		mix.disableNotifications();
	// }
});;
